<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "pb_social".
 *
 * Auto generated 01-09-2016 18:53
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Social Media Streams',
	'description' => '',
	'category' => 'plugin',
	'author' => 'Mikolaj Jedrzejewski - plusB, Ramon Mohi - plusB, Kai Lorenz - plusB, Robert Becker - plusB,  Arend Maubach - plusB',
	'author_email' => 'info@plusb.de',
	'state' => 'stable',
	'uploadfolder' => false,
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '1.3.4',
	'constraints' => 
	array (
		'depends' => 
		array (
			'typo3' => '7.6.0-9.5.99',
			'php' => '5.6.0-7.2.99',
			'extbase' => '',
			'fluid' => '',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
	'clearcacheonload' => false,
	'author_company' => NULL,
);

